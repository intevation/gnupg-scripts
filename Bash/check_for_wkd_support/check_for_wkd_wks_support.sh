#! /bin/bash

declare -a provider_with_wkd
declare -a provider_with_wks
index_wkd=0
index_wks=0

while read line
do
	echo "Testing $line"

	# https://www.gnupg.org/documentation/manuals/gnupg/gpg_002dwks_002dclient.html
	result=$(gpg-wks-client --with-colons --supported $line 2>&1)

	IFS=':' read -ra splitted <<< "$result"

	supports_wkd=${splitted[1]}
	echo "Supports WKD: ${supports_wkd}"
	if [ $supports_wkd -eq 1 ]; then
		provider_with_wkd[$index_wkd]=$line
		index_wkd=$(($index_wkd + 1))
	fi

	supports_wks=${splitted[2]}
	echo "Supports WKS: ${supports_wks}"
	if [ $supports_wks -eq 1 ]; then
		provider_with_wks[$index_wks]=$line
		index_wks=$(($index_wks + 1))
	fi

	error=${splitted[3]}
	if [ "$error" != "" ]; then
		echo "Error-Code: $error"
	fi

	echo ""
done < list_of_popular_domains.txt

echo "Provider which support WKD:"
echo ${provider_with_wkd[*]}

echo ""

echo "Provider which support WKS:"
echo ${provider_with_wks[*]}
