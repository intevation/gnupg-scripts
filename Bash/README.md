# Bash scripts

## Examples

### check_for_wkd_support

Use the [gpg-wks-client](https://www.gnupg.org/documentation/manuals/gnupg/gpg_002dwks_002dclient.html) to check if a provider supports WKD and WKS. The list of tested domains comes from https://github.com/bredele/email-domain-popular. Some provider which are known for privacy-friendly solutions were added to this list.
