# Python scripts

## Requirements

* Python3
* python3-gpg

## More

* [The ‘GnuPG Made Easy’ Reference Manual](https://www.gnupg.org/documentation/manuals/gpgme/)