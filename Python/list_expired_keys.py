#! /usr/bin/python
# -*- coding: utf-8 -*-

import argparse
from datetime import datetime
import gpg
import time

parser = argparse.ArgumentParser(
  description="Lists keys which are expired or will expire soon."
)
parser.add_argument("-d", "--days", metavar="", help="Keys that expire within this range will be listed. Default: 7 days", default=7, type=int)
parser.add_argument("--not-revoked", help="List only keys which are not revoked", action='store_true')
args = parser.parse_args()

limit = 86400 * args.days
now = time.time()

ctx = gpg.Context()
keys = ctx.keylist()
for key in keys:
  for subkey in key.subkeys:
    if args.not_revoked and subkey.revoked != 0:
      continue
    if subkey.expires != 0 and (subkey.expires - now) < limit:
      print("Key-ID: " + str(subkey.keyid))
      print("Fingerprint: " + str(subkey.fpr))
      expiration_date = datetime.utcfromtimestamp(subkey.expires).strftime('%Y-%m-%d %H:%M:%S')
      print("Expiration date: " + expiration_date)
      print("-----------------------------------------------------")
